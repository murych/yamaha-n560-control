# yamaha-n560-control

This is the yamaha-n560-control project.

This is a hobby project made to learn to use KDE's Kirigami platform and Qt's Network, Xml and D-Bus modules.

This project's aim to bring Yamaha's N560 audio reciever remote control application to PC and to openly document it's network API.

## roadmap

- [ ] core library that allows to negotiate with device on the same network
- [ ] kirigami ui

## won't implement / support

- network radio (although mode can be activated)
- spotify support (although mode can be activated)
- other devices from lineup (can be accepted as external contribution)

## less possible features:

- n560's emulator to run software in 'demo' mode
- plasma widget to integrate with system player daemon?

## Building and installing

See the [BUILDING](BUILDING.md) document.

## Contributing

See the [CONTRIBUTING](CONTRIBUTING.md) document.

## Licensing

<!--
Please go to https://choosealicense.com/licenses/ and choose a license that
fits your needs. The recommended license for a project of this type is the
GNU AGPLv3.
-->
