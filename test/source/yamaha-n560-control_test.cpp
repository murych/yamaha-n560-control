#include "lib.hpp"

auto main() -> int
{
  auto const lib = library {};

  return lib.name == "yamaha-n560-control" ? 0 : 1;
}
